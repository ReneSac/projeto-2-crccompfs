
CRCCOMPFS
======

Uma simples extensão do Islenefs com diretórios que verifica a integridade dos dados escritos no arquivo e comprime seus dados usando LZO (limitado a uma página de memória).

O registro de um novo CRC só é feito para a primeira escrita num arquivo. Assim, qualquer modificação por uma escrita subsequente será detectada com alta probabilidade e retornará erro na leitura. Se o conteúdo original do arquivo for restaurado, a leitura poderá transcorrer normalmente.

É usada a função crc32c(u32 crc, const void *address, unsigned int length)  para o cálculo do CRC. É a mesma usada pelo btrfs para sua verificação de consistência. Para usar esse módulo, é necessário definir essa dependência em um arquivo Kconfig na pasta do sistema de arquivos, para que os objetos possam ser ligados corretamente. 

Os dados sobre os quais o CRC será calculado também tem que estar no espaço de memória do kernel, não do usuário, então a chamada à essa função é feita logo depois da escrita, e logo antes de mostrar o resultado de uma leitura. 

O sistema de arquivos não é persistente, então para apagar os dados basta desmontá-lo. O tamanho dos arquivos é limitado à uma página de memória (4kb) o que deve ser  o suficiente para qualquer um. ;)

Para ver as mensagens que o kernel solta, incluindo o CRC do arquivo, use:
dmesg | tail

Para montar o sistema de arquivos:
$ dd if=/dev/zero of=rep bs=1k count=4
$ mkdir -p mnt
$ mount -t crccompfs -o loop rep mnt
$ cd mnt

Instruções de Compilação
-------------------------------------

Para compilar este módulo no kernel, é preciso copiar a pasta crcfs/ para dentro da pasta fs/ do kernel. Além disso, é preciso adicionar a seguinte linha no fs/Makefile, junto com os outros sistemas de arquivo:

obj-y				+= crccompfs/

E a seguinte linha no fs/Kconfig, também onde os outros sistemas de arquivo colocam seu Kconfigs:

source "fs/crccompfs/Kconfig"

O crcfs está selecionado por padrão, mas é bom rodar um make oldconfig/menuconfig para se certificar disso.

shutdown -h 0